# Bash scripting best practices, tips and tricks
### Introduction

### Websites to know
- [Advanced Bash Scripting (ABS)](https://www.tldp.org/LDP/abs/html/)
- [opensource.com](https://opensource.com/)
- [ShellCheck](https://www.shellcheck.net/)

## Summary
1. [Best practices](https://gitlab.com/pchevallereau/bash-scripting-guide/blob/master/README.md#best-practices)
    - [Shebang](https://gitlab.com/pchevallereau/bash-scripting-guide/blob/master/README.md#shebang)
    - [Banner](https://gitlab.com/pchevallereau/bash-scripting-guide/blob/master/README.md#banner)
    - [Invoque subshell](https://gitlab.com/pchevallereau/bash-scripting-guide/blob/master/README.md#invoque-subshell)

2. [Tips](https://gitlab.com/pchevallereau/bash-scripting-guide/blob/master/README.md#tips)
    - [Parsing command-line arguments](https://gitlab.com/pchevallereau/bash-scripting-guide/blob/master/README.md#parsing-command-line-arguments)
    - [Define and call function](https://gitlab.com/pchevallereau/bash-scripting-guide/blob/master/README.md#define-and-call-function)

3. [Tricks](https://gitlab.com/pchevallereau/bash-scripting-guide/blob/master/README.md#tricks)
    - [Tag file descriptor](https://gitlab.com/pchevallereau/bash-scripting-guide/blob/master/README.md#tag-file-descriptor)

## Best practices

#### Shebang
The shebang is always the first line of the script, it define the interpreter which will be used when the script execution
~~~~
#!/usr/bin/env bash
~~~~

#### Banner
The banner is cool for external users, for example they can check and compare their version with the main repository
~~~~
##############################################################
#       script-name.sh
#       Version 1.0.0 - 12/04/2019
#       Author : YOUR NAME
#       Mail: YOUR MAIL
#       Quick script description
##############################################################
~~~~

#### Variable
**Assignment**

`HOSTNAME='test1'` : prefer uppercase for environment and global variables
<br />`val='1' && valList='1,2'` : prefer lowercase and camelCase for local variables
 
**Call**

`echo "${var}"` : as possible, suround variables like this.

**Manipulate**
- **Substitution**
<br />You can substitute characters with variables, here is an example we need to replace the ',' by a space :
~~~~
$ val='toto,toto2'
$ val="${val//,/ }"
$ echo "${val}"
toto toto2
~~~~

- **Default value if empty**
<br />If variable is empty, she are populated with the default value :
~~~~
# Example with empty variable
  $ defaultValue='ERROR'
  $ val=''
  $ val="${val:-$defaultValue}"
  $ echo "${val}"
  ERROR # The variable was populated with default value because she is empty

# Example with populated variable
  $ defaultValue='ERROR'
  $ val='toto'
  $ val="${val:-$defaultValue}"
  $ echo "${val}"
  toto # The variable was not populated with default value because she was non-empty
~~~~

This is usefull on script, you can easily detect errors

#### Invoque subshell
Prefer use this syntax `"$(cmdToExec)"` instead backquote
~~~~
$ var="$(date)"
$ echo "${var}"
samedi 11 mai 2019, 11:36:03 (UTC+0200)
~~~~

## Tips

#### Parsing command-line arguments
**Wiki:**

- "${#}" = number of passed arguments
- "${@}" = list of all arguments

**Code:**
~~~~
#!/usr/bin/env bash
if [[ "${#}" -gt 0  ]] ; then   
  for opts in "${@}"; do
    case "${opts}" in
      --silent|-s)        # Argument without value { long | short }
      ;;
      --user=*|-u=*)      # Argument with value {long | short}
        user=${opts##*=}
        echo "$user"
      ;;
      *)                  # Match everything if not found before
        echo "error : ${opts}"
      ;;
    esac
  done
fi
~~~~

**How to use:**

1. Write code in test.sh
2. Make it executable `chmod +x test.sh`
3. Run
~~~~
$ ./test.sh --user=TOTO
TOTO
~~~~

#### Define and call function
I like named my function with an underscore at start, because this increase functions identification and code comprehension.

**Code:**
~~~~
#!/usr/bin/env bash

# Define function
_helloWorld()
{
  echo 'Hello World'
}

# Call function
_helloWorld
~~~~

**How to use:**
1. Write code in test.sh
2. Make it executable `chmod +x test.sh`
3. Run
~~~~
$ ./test.sh
Hello World
~~~~

## Tricks

#### Tag file descriptor

This, tag the filedescriptor 2 with the flag "[ERR] ".
<br />So, all messages passed to the file descriptor 2 are tagged and gived to file descriptor 1(screen)

Warning:
<br />With debug mode `set -x`, you need to change his default file descriptor by file descriptor 1 `export BASH_XTRACEFD=1`.
<br />If not, all debug messages will be also tagged.

**Code:**
~~~~
#!/usr/bin/env bash

# File descriptors tag method
exec 2> >(while read line; do echo -e "\e[01;31m[ERR] $line\e[0m" >&2; done)

# Debug mode
export BASH_XTRACEFD=1
set -x

# Some commands for test
ls /        # Cmd without error
unknownCMD  # Cmd with error
tyzdisz     # Cmd with error
echo "bon"  # Cmd without error
~~~~

**How to use:**
1. Write code in test.sh
2. Make it executable `chmod +x test.sh`
3. Run
~~~~
$ ./test.sh
+ ls /
bin   etc	  initrd.img.old  lost+found  opt   run   sys  var
boot  home	  lib		  media       proc  sbin  tmp  vmlinuz
dev   initrd.img  lib64		  mnt	      root  srv   usr  vmlinuz.old
+ unknownCMD
[ERR] ./test.sh: ligne 15: unknownCMD : commande introuvable
+ tyzdisz
[ERR] ./test.sh: ligne 16: tyzdisz : commande introuvable
+ echo bon
bon
~~~~
